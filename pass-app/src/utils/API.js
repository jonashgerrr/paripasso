import axios from "axios";

const API = axios.create({
  baseURL: process.env.REACT_APP_ENDPOINT,
});

// Add a request interceptor
API.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
API.interceptors.response.use(
  (succ) => Promise.resolve(succ),
  (err) => {
    return Promise.reject(err);
  }
);

export default API;