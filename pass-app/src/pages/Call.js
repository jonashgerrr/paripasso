import { useState } from "react";
import styled from "styled-components";
import { Container, Item, Button, Title } from "../components/styles";
import API from '../utils/API'

const Senha = styled.div`

display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    `

const Call = () => {
    const [password, setPassword] = useState('');


    const newPassword = async (type) => {
        const { data } = await API.get(`password/${type}`)
        setPassword(data.password);

        setTimeout(() => {
            setPassword('')
        }, 3000)
    }

    return <>
        {password ?
            <Container>
                <Senha> <span>  Sua senha é    </span><Title> {password}</Title></Senha>

            </Container>
            : <Container>
                <Item>
                    <Button onClick={() => newPassword('N')}>Normal</Button>
                </Item>

                <Item>
                    <Button onClick={() => newPassword('P')}>Prioritário</Button>
                </Item>
            </Container>}

    </>
}

export default Call;