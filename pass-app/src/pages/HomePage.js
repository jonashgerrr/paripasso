import { useHistory } from "react-router-dom";
import { Container,Item,Button } from '../components/styles'

const HomePage = () => {
    const history = useHistory();

    return <>
        <Container>

            <Item>
                <Button onClick={() => history.push('/call')}>
                    Senhas
                </Button>
            </Item>
            <Item>
                <Button onClick={() => history.push('/password')}>
                    Gerente
                </Button>
            </Item>
            <Item>
                <Button onClick={() => history.push('/painel')}>
                    Painel
                </Button>
            </Item>
        </Container>
    </>
}

export default HomePage;