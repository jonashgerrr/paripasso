import { useEffect, useState } from "react";
import styled from "styled-components";
import { Button, ButtonSmall, Container, Item } from "../components/styles";
import API from "../utils/API";

const Lista = styled.ul`
`

const Password = () => {
    const [passwords, setPasswords] = useState([]);

    const next = () => {
        API.get('call').then(() => {
            loadData()
        })
    }

    const reset = () => {
        API.post('reset').then(() => {
            loadData();
            alert('Contagem de senhas reiniciada com sucesso')
        })
    }

    const loadData = () => {
        API.get('password').then(({ data }) => {
            setPasswords(data.passwords)
        })
    }

    useEffect(() => {
        loadData()
    }, [])

    return <>
        <Container>
            <Item>
                <Button onClick={next}>Chamar próximo</Button>
                <ButtonSmall onClick={reset}>Reiniciar Contagem</ButtonSmall>
            </Item>
            <Item>
                
            </Item>
            <Item>
                <Lista>
                    Pessoas na fila
                    {passwords?.map((item) => {
                        return <li>{item.password}<br /></li>
                    })}
                </Lista>
            </Item>
        </Container>
    </>
}

export default Password;