import { useEffect, useState } from "react";
import styled from "styled-components";
import { Button, Container, Item } from "../components/styles";
import API from "../utils/API";

const Lista = styled.ul`
`

const Painel = () => {
    const [passwords, setPasswords] = useState([]);

    const loadData = () => {
        API.get('calleds').then(({ data }) => {
            setPasswords(data)
        }).finally(() => setTimeout(loadData, 2000))
    }

    useEffect(() => {
        loadData()
    }, [])

    return <>
        <Container>
            <Lista>
                <span>Próximo</span>
                <b> <li><h1>{passwords[0]?.password}</h1></li></b>
                <span>Últimos 3</span>
                <li>{passwords[1]?.password}</li>
                <li>{passwords[2]?.password}</li>
                <li>{passwords[3]?.password}</li>
            </Lista>
        </Container>
    </>
}

export default Painel;