import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Call from "./pages/Call";
import Password from "./pages/Password";
import Painel from "./pages/Painel";
import HomePage from "./pages/HomePage";

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/call">
            <Call />
          </Route>
          <Route path="/password">
            <Password />
          </Route>
          <Route path="/painel">
            <Painel />
          </Route>
          <Route path="/" exact>
            <HomePage />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
