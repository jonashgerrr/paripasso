import styled from "styled-components";

export const Container = styled.div`
display: flex;
height: 100%;
justify-content: space-evenly;
`

export const Button = styled.button`
    background-color: #48F056;
    border: none;
    color: white;
    padding: 15px 32px;
    margin: 5px 10px;
    text-align: center;
    -webkit-text-decoration: none;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor: pointer;
`

export const ButtonSmall = styled.button`
    color: #48F056;
    border: none;
    background-color: white;
    padding: 5px 12px;
    margin: 5px 10px;
    text-align: center;
    -webkit-text-decoration: none;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    cursor: pointer;
`

export const Item = styled.div`
width: 100%;

flex-direction: column;
display: flex;
    justify-content: center;
`
export const Title = styled.h2`
font-size: 2.5em;
margin: 0;`