package com.github.jonashger.pass.dto;

import java.util.Date;

import com.github.jonashger.pass.enumerador.PasswordTypeEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnPasswordDTO {

	private String password;
	private PasswordTypeEnum passwordTypeEnum;
	private Date date;

}
