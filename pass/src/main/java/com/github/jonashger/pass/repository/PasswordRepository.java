package com.github.jonashger.pass.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.github.jonashger.pass.entity.Password;
import com.github.jonashger.pass.enumerador.PasswordTypeEnum;

@Repository
public interface PasswordRepository extends JpaRepository<Password, Long> {

	@Query("Select p from Password p where type = :type order by p.on desc ")
	public abstract List<Password> getLastPassword(PasswordTypeEnum type, Pageable page);

	@Query("Select p from Password p where p.calledOn is null order by  type desc,p.on asc ")
	public abstract List<Password> getNext(Pageable page);

	@Transactional
	@Modifying
	@Query("delete from Password p where p.calledOn is not null")
	public abstract void deleteAllWhenCalledOnIsNotNull();

	@Query("Select count(p) from Password p where p.calledOn is null")
	public abstract long getCountAtivos();

	@Query("Select p from Password p where p.calledOn is not null order by p.calledOn desc ")
	public abstract List<Password> getCalleds(PageRequest of);

}
