package com.github.jonashger.pass.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.jonashger.pass.dto.PageablePasswordDTO;
import com.github.jonashger.pass.dto.ReturnPasswordDTO;
import com.github.jonashger.pass.enumerador.PasswordTypeEnum;

@CrossOrigin(origins = "*")
public interface PasswordController {

	@GetMapping("/password/{priority}")
	abstract ResponseEntity<ReturnPasswordDTO> generateNewPassword(
			@PathVariable("priority") PasswordTypeEnum passwordTypeEnum);

	@GetMapping("/call")
	abstract ResponseEntity<ReturnPasswordDTO> callNext();

	@PostMapping("/reset")
	abstract ResponseEntity<Void> reset();

	@GetMapping("/calleds")
	abstract ResponseEntity<List<ReturnPasswordDTO>> getCalled();

	@GetMapping("/password")
	abstract ResponseEntity<PageablePasswordDTO> getWaiting(@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "limit", defaultValue = "10") int limit);
}
