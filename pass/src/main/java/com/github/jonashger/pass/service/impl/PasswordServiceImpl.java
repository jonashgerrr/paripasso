package com.github.jonashger.pass.service.impl;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.github.jonashger.pass.dto.PageablePasswordDTO;
import com.github.jonashger.pass.dto.ReturnPasswordDTO;
import com.github.jonashger.pass.entity.Password;
import com.github.jonashger.pass.enumerador.PasswordTypeEnum;
import com.github.jonashger.pass.repository.PasswordRepository;
import com.github.jonashger.pass.service.PasswordService;

@Service
public class PasswordServiceImpl implements PasswordService {

	@Autowired
	@Lazy
	private PasswordRepository passwordRepository;

	private final Long INITIAL_NUMBER = 0L;
	private final Long PRIORITY_NUMBER = 7999L;

	AtomicLong atomicNormal = new AtomicLong(INITIAL_NUMBER);
	AtomicLong atomicPriority = new AtomicLong(PRIORITY_NUMBER);
	AtomicLong sequence = new AtomicLong(0);

	@Override
	public ReturnPasswordDTO generateNewPassword(PasswordTypeEnum passwordTypeEnum) {
		ReturnPasswordDTO returnPasswordDTO = new ReturnPasswordDTO();

		if (atomicNormal.get() == INITIAL_NUMBER && atomicPriority.get() == PRIORITY_NUMBER && sequence.get() == 0) {
			List<Password> passwordOptional = passwordRepository.getLastPassword(passwordTypeEnum,
					PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "on")));

			if (!passwordOptional.isEmpty()) {
				if (passwordTypeEnum.equals(PasswordTypeEnum.N)) {
					atomicNormal.set(passwordOptional.get(0).getSequence());
				} else {
					atomicPriority.set(passwordOptional.get(0).getSequence());
				}
			}

		}
		sequence.addAndGet(1);

		generateNewPassword(passwordTypeEnum, returnPasswordDTO,
				passwordTypeEnum.equals(PasswordTypeEnum.N) ? atomicNormal : atomicPriority);

		returnPasswordDTO.setPasswordTypeEnum(passwordTypeEnum);

		return returnPasswordDTO;
	}

	private void generateNewPassword(PasswordTypeEnum passwordTypeEnum, ReturnPasswordDTO returnPasswordDTO,
			AtomicLong sequence) {
		Password save = passwordRepository.save(new Password(passwordTypeEnum, sequence.getAndIncrement()));
		returnPasswordDTO.setPassword(save.getPassword());
		returnPasswordDTO.setDate(save.getOn());

	}

	@Override
	public synchronized ReturnPasswordDTO callNext() {
		List<Password> passwordOptional = passwordRepository.getNext(PageRequest.of(0, 1));
		if (passwordOptional.isEmpty())
			return null;

		Password password = passwordOptional.get(0);
		password.setCalledOn(new Date());
		passwordRepository.save(password);

		return new ReturnPasswordDTO(password.getPassword(), password.getType(), password.getOn());

	}

	@Override
	public void reset() {
		atomicNormal.set(INITIAL_NUMBER);
		atomicPriority.set(PRIORITY_NUMBER);
		// passwordRepository.deleteAllWhenCalledOnIsNotNull();

	}

	@Override
	public PageablePasswordDTO getWaiting(int page, int limit) {
		List<Password> passwordOptional = passwordRepository.getNext(PageRequest.of(page, limit));

		PageablePasswordDTO pageablePasswordDTO = new PageablePasswordDTO();
		pageablePasswordDTO.setPasswords(passwordOptional.stream()
				.map(item -> new ReturnPasswordDTO(item.getPassword(), item.getType(), item.getOn()))
				.collect(Collectors.toList()));

		pageablePasswordDTO.setPages(passwordRepository.getCountAtivos() / limit);
		return pageablePasswordDTO;
	}

	@Override
	public List<ReturnPasswordDTO> getCalled() {
		List<Password> passwordOptional = passwordRepository
				.getCalleds(PageRequest.of(0, 4, Sort.by(Sort.Direction.DESC, "calledOn")));
		return passwordOptional.stream()
				.map(item -> new ReturnPasswordDTO(item.getPassword(), item.getType(), item.getOn()))
				.collect(Collectors.toList());
	}

}
