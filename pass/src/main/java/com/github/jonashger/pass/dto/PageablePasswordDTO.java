package com.github.jonashger.pass.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageablePasswordDTO {

	private long pages;
	private List<ReturnPasswordDTO> passwords;

}