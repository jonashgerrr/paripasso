package com.github.jonashger.pass.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.github.jonashger.pass.controller.PasswordController;
import com.github.jonashger.pass.dto.PageablePasswordDTO;
import com.github.jonashger.pass.dto.ReturnPasswordDTO;
import com.github.jonashger.pass.enumerador.PasswordTypeEnum;
import com.github.jonashger.pass.service.PasswordService;

@Controller
public class PasswordControllerImpl implements PasswordController {

	@Autowired
	@Lazy
	private PasswordService passwordService;

	@Override
	public ResponseEntity<ReturnPasswordDTO> generateNewPassword(PasswordTypeEnum passwordTypeEnum) {
		return ResponseEntity.ok(this.passwordService.generateNewPassword(passwordTypeEnum));
	}

	@Override
	public ResponseEntity<ReturnPasswordDTO> callNext() {
		return ResponseEntity.ok(this.passwordService.callNext());
	}

	@Override
	public ResponseEntity<Void> reset() {
		this.passwordService.reset();
		return ResponseEntity.noContent().build();
	}

	@Override
	public ResponseEntity<PageablePasswordDTO> getWaiting(int page, int limit) {
		return ResponseEntity.ok(this.passwordService.getWaiting(page, limit));
	}

	@Override
	public ResponseEntity<List<ReturnPasswordDTO>> getCalled() {
		return ResponseEntity.ok(this.passwordService.getCalled());
	}

}
