package com.github.jonashger.pass.service;

import java.util.List;

import com.github.jonashger.pass.dto.PageablePasswordDTO;
import com.github.jonashger.pass.dto.ReturnPasswordDTO;
import com.github.jonashger.pass.enumerador.PasswordTypeEnum;

public interface PasswordService {

	abstract ReturnPasswordDTO generateNewPassword(PasswordTypeEnum passwordTypeEnum);

	abstract ReturnPasswordDTO callNext();

	abstract void reset();

	abstract PageablePasswordDTO getWaiting(int page, int limit);

	abstract List<ReturnPasswordDTO> getCalled();

}
