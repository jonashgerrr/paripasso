package com.github.jonashger.pass.enumerador;

public enum PasswordTypeEnum {
	N("N", 0l), P("P", 7999l);

	private long initial;
	private String type;

	PasswordTypeEnum(String type, long initial) {
		this.initial = initial;
		this.type = type;
	}

	public long getInitial() {
		return this.initial;
	}

	public String toString() {
		return this.type;
	}

}
