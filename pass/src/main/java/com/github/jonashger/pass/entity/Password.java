package com.github.jonashger.pass.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.github.jonashger.pass.enumerador.PasswordTypeEnum;
import com.github.jonashger.pass.util.StringUtil;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "password")
@Data
@NoArgsConstructor
public class Password {

	@Id
	@Column(name = "password_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private PasswordTypeEnum type;

	@Column(name = "sequence")
	private Long sequence;

	@Column(name = "password")
	private String password;

	@Column(name = "date_on")
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date on;

	@Column(name = "called_on", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date calledOn;

	public Password(PasswordTypeEnum type, long sequence) {
		super();
		this.type = type;
		this.sequence = sequence + 1;
		this.password = type.toString().concat(StringUtil.padLeftZeros(this.sequence.toString(), 4));
	}

}
